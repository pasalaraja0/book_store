<%-- 
    Document   : BookStore
    Created on : 7 Aug, 2019, 8:43:15 PM
    Author     : lenovo-pc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Base64"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <title>JSP Page</title>
        <style>
        .fade {
            opacity: 1;
        }
        .mybtn
        {
            background-color: #bc3232 !important;
        }
            
        </style>
    </head>
    <body>
        <div id="navcolor"><h1 align="center" style="color:white">Book Store</h1>
          
        </div>
        
        <%
                Class.forName("com.mysql.cj.jdbc.Driver");
                String id = request.getParameter("categoryId");
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/onshop","root","1234");
                System.out.println("result"+conn);
                String categoryid  = null;
                        
                      categoryid  = request.getParameter("categoryId");
                String qry = null;
                if(categoryid != null)
                {
                    int catId = Integer.parseInt(id);
                    qry = "select BOOK_ID,BOOK_NAME,AUTHOR_NAME,CATEGORY_ID,image,no_of_books_sold,BOOK_PRICE,TOTAL_NO_OF_BOOKS,AVAILABLE_BOOKS,BOOKDESCRIPTION from books121 where CATEGORY_ID="+catId;
                }
                else
                {
                  qry = "select BOOK_ID,BOOK_NAME,AUTHOR_NAME,CATEGORY_ID,image,no_of_books_sold,BOOK_PRICE,TOTAL_NO_OF_BOOKS,AVAILABLE_BOOKS,BOOKDESCRIPTION from books121";  
                }
                Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(qry);
               System.out.println("result"+rs);
%>

        <div id="mainpageId">                                                   
<div id="displayimage">
    <table id="booksdetailsTableId" class="table table-bordered" style="width:66.3%;margin-left: 138px;">
      <tbody>
         <% while(rs.next()){  %>
        <% byte[] imgData = rs.getBytes("IMAGE"); 
        System.out.println("bytes image: "+imgData);
        if(imgData != null)
        {
            String encode = Base64.getEncoder().encodeToString(imgData);
            request.setAttribute("imgBase", encode);
        }
         String iconId = "Icon"+rs.getString("BOOK_ID");
          String BtnId = "btn"+rs.getString("BOOK_ID");
           String bookId = rs.getString("BOOK_ID");
           String b_name = rs.getString("BOOK_NAME");
         System.out.println("***iconId"+iconId);
        %>
        <tr>
            <td>
                <img src="data:image/jpeg;base64,${imgBase}" alt="" width="210"
			height="200" class="image"/></td>
            <td><%=rs.getString("BOOK_NAME")%></br><%=rs.getString("AUTHOR_NAME")%></br><b><%=rs.getString("BOOK_PRICE")%>Rs</b></br><%=rs.getString("BOOKDESCRIPTION")%></td>
            <td ><a  href="#" class="btn btn-primary a-btn-slide-text mybtn" onclick="modal();">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    <span><strong>Add Cart</strong></span>          
                </a> <i class="fa fa-check-circle text-success"  style="display:none;" id=<%=iconId%>></i> </td>
            
        </tr>
                
          <%}%>  
      </tbody> 
    </table>
      <div class="modal fade" id="myModal" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
          <h4 class="modal-title">Login</h4>
        </div>
        <div class="modal-body">
          <p>Please Login.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="homeFunc();">Close</button>
        </div>
      </div>
      
  
      <style>
          #navcolor
            {
                background-color:#bc3232 !important;
                    height: 50px;
            }
            
          </style>
          <script>
              function modal()
              {
                  //document.getElementById("myModal").style.display="block";
                  $('#myModal').modal('show');
              }
              
function homeFunc()
 {
     var jspcall = "index.html";
            window.location.href=jspcall;
 }
          </script>

    </body>
</html>

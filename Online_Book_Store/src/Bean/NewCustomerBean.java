 package Bean;
public class NewCustomerBean {
	private String userName;
	private String password;
	private String email_Id;
	private String phonenumber;
	private String confirmPassword;
	public String getUserName() {
		return userName;
	}
	public void setUsername(String username) {
		this.userName = username;	
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmailid() {
		return email_Id;
	}
	public void setEmailid(String email_Id) {
		this.email_Id = email_Id;
	}
	public String getPhoneNumber() {
		return phonenumber;
	}
	public void setPhoneNumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getConfirmPassword() {
		return confirmPassword; 
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
		System.out.println("***SRT**** confirmPassword: "+confirmPassword);
	}

}

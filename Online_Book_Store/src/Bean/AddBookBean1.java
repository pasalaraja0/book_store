package Bean;

import javax.servlet.http.Part;

public class AddBookBean1 {
	
	private String bookName;
	private String authorName;
	private String price;
	private String categoryId;
	private Part image;
        private String totalBooks;
        private String booksSold;
        private String availableBooks;
        private String bookDescription;
        
        public String getBookDescription() {
		return bookDescription;
	}
	public void setBookDescription(String bookDesc) {
		this.bookDescription = bookDesc;	
	}
	
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookname) {
		this.bookName = bookname;	
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorname) {
		this.authorName = authorname;	
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;	
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setTotalBooks(String tBooks) {
		this.totalBooks = tBooks;	
	}
	public String getTotalBooks() {
		return totalBooks;
	}
	public void setCategoryId(String category) {
		this.categoryId = category;	
	}
	public Part getImage() {
		return image;
	}
	public void setImage(Part image) {
		this.image = image;	
	}
        public void setAvailableBooks(String availBooks) {
		this.availableBooks = availBooks;	
	}
	public String getAvailableBooks() {
		return availableBooks;
        }
          public void setSoldBooks(String soldBooks) {
		this.booksSold = soldBooks;	
	}
	public String getSoldBooks() {
		return totalBooks;
        }
}
	




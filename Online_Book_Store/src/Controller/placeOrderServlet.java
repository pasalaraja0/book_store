/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import DB.DbConnection;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import net.sf.json.JSONObject;
//import net.sf.json.JSONSerializer;
//import net.sf.json.JSONSerializer

/**
 *
 * @author lenovo-pc
 */
@WebServlet(name = "placeOrderServlet", urlPatterns = {"/placeOrderServlet"})
public class placeOrderServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet placeOrderServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet placeOrderServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String greetings = "Hello ";
        response.setContentType("text/plain");
        response.getWriter().write(greetings);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String[] myJsonData = request.getParameterValues("cartData");
        String customerId=request.getParameter("customerId");
        System.out.println("HUge success customerId:"+customerId);
       ArrayList<JSONObject> listdata = new ArrayList<JSONObject>();     
JSONArray jArray = null;       
try {
             jArray = new JSONArray(myJsonData[0]);
        } catch (JSONException ex) 
        {
            Logger.getLogger(placeOrderServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    for (int i=0; i<jArray.length(); i++) 
    {
            try {
                listdata.add( (JSONObject)jArray.get(i));
            } catch (JSONException ex) {
                Logger.getLogger(placeOrderServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
   System.out.println("***FINAI listdata****"+listdata); 
   System.out.println("***FINAI listdata Size****"+listdata.size()); 
   ArrayList < JSON2Apex > finalDataList = new ArrayList< JSON2Apex >();
   int totalCartAmount = 0;
    for(Object obj :  listdata)
    {
        JSON2Apex yourHashMap = new Gson().fromJson(obj.toString(), JSON2Apex.class);
        totalCartAmount = totalCartAmount + yourHashMap.b_total;
        finalDataList.add(yourHashMap);
    }
   System.out.println("***finalDataList listdata****"+listdata); 
   System.out.println("***FINAI finalDataList Size****"+listdata.size()); 
   System.out.println("***totalCartAmount****"+totalCartAmount); 
Connection conn = null;
try{

conn = DbConnection.conn();
Random rand = new Random(); 
DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	LocalDate localDate = LocalDate.now();
	System.out.println(dtf.format(localDate)); //2016/11/16
        Date now = new Date(); 
        java.sql.Date sqlDate = new java.sql.Date(now.getTime());
        System.out.println("***now time****"+now); 
        System.out.println("***sqlDate****"+sqlDate); 
        // Generate random integers in range 0 to 999 
        int rand_int1 = rand.nextInt(1000); 
       // int rand_int2 = rand.nextInt(1000); 
  
    String qry="insert into orders(CUSTOMER_ID,ORDER_STATUS,TOTAL_AMOUNT,ORDER_PLACED_DATE) values (?,?,?,?)";
    PreparedStatement st = conn.prepareStatement(qry);
    
    st.setInt(1,rand_int1);
    st.setInt(2,Integer.parseInt(customerId));
    st.setString(3,"ordered");
    st.setInt(4,totalCartAmount);
    st.setDate(5, sqlDate);
   int i = st.executeUpdate();
   System.out.println("Inserted STATUS: "+i);
    if(i > 0)
    {
        System.out.println("Order successfully Inserted");
        String orderItemqry="insert into orderitems(ORDERITEM_ID,BOOK_ID,ORDERITEM_QUANTITY,ORDERITEM_PRICE,ORDER_ID,BOOKNAME) values (?,?,?,?,?,?)";
    try
    {
       Connection conn1 = null;
conn1 = DbConnection.conn();
String SQL_SELECT = "SELECT order_id FROM ORDERS where ORDER_ID="+rand_int1;
PreparedStatement orderstmt = conn1.prepareStatement(SQL_SELECT);
        ResultSet rs = orderstmt.executeQuery();
       int orderId= 0;
        if (rs.next()) 
        {
             orderId = rs.getInt("order_id");
        }
           PreparedStatement Itemst = conn1.prepareStatement(orderItemqry);
        int j = 1;
        for(JSON2Apex mapval : finalDataList)
        {
        Itemst.setInt(1,j);
        Itemst.setInt(2,mapval.b_id);
        Itemst.setInt(3,mapval.b_count);
        Itemst.setInt(4,mapval.b_price);
        Itemst.setInt(5,orderId);
        Itemst.setString(6,mapval.b_name);
        Itemst.addBatch();
        j++;
        }
        int[] updateCounts = Itemst.executeBatch();
        conn1.commit();
        
        for (int k=0; k<updateCounts.length; k++) {
        if (updateCounts[k] >= 0) {
        System.out.println("OK; updateCount="+updateCounts[k]);
        }
        else if (updateCounts[k] == Statement.SUCCESS_NO_INFO) {
        System.out.println("OK; updateCount=Statement.SUCCESS_NO_INFO");
        }
        else if (updateCounts[k] == Statement.EXECUTE_FAILED) {
        System.out.println("Failure; updateCount=Statement.EXECUTE_FAILED");
        }
        }
    }
 catch(Exception e1)
 {
     System.out.println("****EXCEPTION*****Order Items successfully Inserted"+e1.getMessage()); 
 }
        
    }
}
catch(Exception e)
{
    
}
    String greetings = "Hello ";
        response.setContentType("text/plain");
        response.getWriter().write(greetings); 
  }//method


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
public class JSON2Apex{

public Integer b_id;
public Integer b_price;
public String b_name;
public Integer b_count;
public Integer b_total;

}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import Bean.AddBookbean;
import Dao.AddBookDao;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
@WebServlet(name = "AddBookServlet", urlPatterns = {"/AddBook"})
@MultipartConfig(maxFileSize = 16177216)
public class AddBookServlet extends HttpServlet {
private static final long serialVersionUID = 1L;
    
/* protected void doPost(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException {

}*/

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /* @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    processRequest(request, response);
    }*/

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       System.out.println("books ervlet called");
       InputStream inputStream = null; // input stream of the upload file
         // InputStream inputStream = null; // input stream of the upload file
          
       String bookName = request.getParameter("Bookname").trim();
        String authorName = request.getParameter("authorName").trim();
        String categoryId = request.getParameter("category").trim();
        String bookdescription = request.getParameter("bookDescription").trim();
        String totalBooks = request.getParameter("TotalNoOfBooks").trim();
        String booksSold = request.getParameter("NoOfBooksSold").trim();
        String price = request.getParameter("BookPrice").trim();
        String availableBooks = request.getParameter("AvailableBooks").trim();
Part part = request.getPart("image");
 System.out.println("part: "+part.getName());
 inputStream = part.getInputStream();
        System.out.println("bookName: "+bookName);
        System.out.println("authorName: "+authorName);

        System.out.println("categoryId: "+categoryId);
        System.out.println("bookdescription: "+bookdescription);

        System.out.println("totalBooks: "+totalBooks);
        System.out.println("booksSold: "+booksSold);

        System.out.println("availableBooks: "+availableBooks);
        AddBookbean instance = new AddBookbean();
		instance.setbookName(bookName);
		instance.setauthorName(authorName);
                instance.setcategoryId(categoryId);
                instance.setBookPrice(price);
		instance.setbookdescription(bookdescription);
		instance.settotalBooks(totalBooks);
                instance.setbooksSold(booksSold);
                instance.setavailableBooks(availableBooks);
                instance.setBytes(inputStream);
             AddBookDao bookDao = new AddBookDao();
             String returnResponse = bookDao.user(instance);
            System.out.println("Add Book connection return in servlet "+returnResponse); 
            if(returnResponse.equals("SUCCESS")) //If function returns success string then user will be rooted to Home page
			 {
			request.getRequestDispatcher("/AddBook.jsp").forward(request, response);
            }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import DB.DbConnection;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author lenovo-pc
 */
@WebServlet(name = "Save_PersonalSetting", urlPatterns = {"/Save_PersonalSetting"})
public class Save_PersonalSetting extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Save_PersonalSetting</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Save_PersonalSetting at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String[] myJsonData = request.getParameterValues("customerDetails");
        System.out.println("myJsonData: "+myJsonData);
        System.out.println("myJsonData first: "+myJsonData[0]);
        JSONArray jArray = null;    
        String custId = null;
        String name = null;
        String phone = null;
try {
             jArray = new JSONArray(myJsonData[0]);
        } catch (JSONException ex) 
        {
         System.out.println("Error in json Array: "+myJsonData);  
    }
 System.out.println("jArray first: "+jArray);
 System.out.println("jArray first: "+jArray.length());
        try {
           
             custId = (String) jArray.get(0);
         name = (String)jArray.get(2);
         phone = (String)jArray.get(1);
        } catch (JSONException ex) {
            Logger.getLogger(Save_PersonalSetting.class.getName()).log(Level.SEVERE, null, ex);
        }
         System.out.println("custId first: "+custId);
          System.out.println("name first: "+name);
           System.out.println("phone first: "+phone);
           Connection conn = null;
           try{
               conn = DbConnection.conn();
               System.out.println("connection success ");
                String qry="update customers set USERNAME=?  WHERE CUSTOMER_ID = ?";
 //// System.out.println("query success "+qry);num
      PreparedStatement st = conn.prepareStatement(qry);
      /* System.out.println("before 1 success "+Integer.parseInt(phone));
      long p = Integer.parseInt(phone);
      long q = 9999999999l;
      long num = Long.parseLong(phone);
      //System.out.println("1 success "+p);
      //st.setInt(1,p);
      st.setLong(1,num);*/
    // System.out.println("1 success "+p);
    st.setString(1,name);
   // st.setString(2,"thahaseenUpdate");
    // System.out.println("before execute update success ");
   int cusId = Integer.parseInt(custId);
    st.setInt(2,cusId);
   int i = st.executeUpdate();
   System.out.println("final update: "+i);
           }
           catch(Exception e12)
           {
               System.out.println("Error final update: "+e12.getMessage());
                System.out.println("Error final update: "+e12.getCause());
           }
           
    }
/*for(int i=0; i<jArray.length(); i++){
    listData.add(jArray.get(i));
}*/

    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    public class JSON2Apex{

public Integer customer_Id;
public String email;
public String phone;
public Integer name;
//public Integer b_total;

}

}

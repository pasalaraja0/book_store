/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import Bean.AddBookbean;
import Bean.AddCategoryBean;
import Dao.AddBookDao;
import Dao.AddCategoryDao;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author lenovo-pc
 */
@WebServlet(name = "AddCategoryServlet", urlPatterns = {"/AddCategory"})
public class AddCategoryServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       System.out.println("books ervlet called");
       String categoryName = request.getParameter("Categoryname");
               System.out.println("11111111111111111111 "+categoryName);

        String categoryId = request.getParameter("categoryid");
        System.out.println("bookName: "+categoryName);
        System.out.println("categoryId: "+categoryId);
        AddCategoryBean instance = new AddCategoryBean();
		
		instance.setCategoryName(categoryName);
                instance.setCategoryId(categoryId);
             AddCategoryDao categoryDao = new AddCategoryDao();
             String returnResponse = categoryDao.user(instance);
            System.out.println("Add Book connection return in servlet "+returnResponse); 
            if(returnResponse.equals("SUCCESS")) //If function returns success string then user will be rooted to Home page
			 {
			request.getRequestDispatcher("/AddCategory.jsp").forward(request, response);
            }
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

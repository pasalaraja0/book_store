package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

import javax.servlet.http.Part;

import Bean.AddBookBean1;
import DB.DbConnection;

public class AddBookDao1 {
	public String book(AddBookBean1 addbookBean)
	{
		String bookName=addbookBean.getBookName();
		String authorName=addbookBean.getAuthorName();
		String soldBooks=addbookBean.getSoldBooks();
		//String price=addbookBean.getPrice();
                int price=100;
                String availableBooks=addbookBean.getAvailableBooks();
                String bookDescription=addbookBean.getBookDescription();
                String totalBooks=addbookBean.getTotalBooks();
		String category=addbookBean.getCategoryId();
		Part image=addbookBean.getImage();
                //String category=addbookBean.getTotalBooks();
		Connection conn = null;
		System.out.println("getting values");
		try {
			conn = DbConnection.conn();
			System.out.println("before111");
			
			String qry="insert into books121(book_id,book_name,author_name,category_id,image,bookdescription,book_price,total_no_of_books,no_of_books_sold,available_books) values (?,?,?,?,?,?,?,?,?)";
			System.out.println("after2222");
			PreparedStatement st = conn.prepareStatement(qry);
			st.setString(1, bookName);
			st.setString(2, authorName);
			st.setString(3, category);
			st.setString(5, bookDescription);
			st.setInt(6, price);
                        st.setString(7, totalBooks);
                        st.setString(8, soldBooks);
                        st.setString(9, availableBooks);
                        
                        
			System.out.println("after set name");
			st.setBinaryStream(4, image.getInputStream(), (int)  image.getSize());
			System.out.println("after set image");
			
			st.executeUpdate();
			System.out.println("inserted");
			return "SUCCESS";
		
		}
		catch(Exception e){
		System.out.println("add book dao 1 in catch"+e.getMessage());
		e.printStackTrace();
		
		return "UNSUCCESS";
		}
}


}

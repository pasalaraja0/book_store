package Dao;



import java.sql.Connection;
import java.sql.PreparedStatement;
import Bean.NewCustomerBean;
import DB.DbConnection;

public class NewCustomerDao {
	public String authenticateUser(NewCustomerBean userBean)
	{
		String userName=userBean.getUserName();
		String password=userBean.getPassword();
		String emaiId=userBean.getEmailid();
		String phoneNumber=userBean.getPhoneNumber();
		String confirmPassword=userBean.getConfirmPassword();
		
		Connection conn = null;
		System.out.println("getting values");
		try {
			conn = DbConnection.conn();
			System.out.println("before111"+conn);
			String qry="insert into customers(email_id,phonenumber,username,password,confirm_password)"
					+ " values (?,?,?,?,?)";
			System.out.println("after2222");
			PreparedStatement st = conn.prepareStatement(qry);
			st.setString(1, emaiId);
			st.setString(2, phoneNumber);
			st.setString(3, userName);
			st.setString(4, password);
			st.setString(5, confirmPassword);
			st.executeUpdate();
			System.out.println("inserted");
			return "SUCCESS";
		
		}
		catch(Exception e){
			System.out.println("Error: "+e.getMessage());
			System.out.println("Error trace: "+e.getStackTrace());
		}
		return "UNSUCCESS";
		
}
}



package Dao;

import Bean.AddBookbean;
import DB.DbConnection;
import static DB.DbConnection.conn;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import DB.DbConnection;
import java.io.InputStream;

public class AddBookDao {
    
    public ResultSet resultset = null;
    ///public Connection conn=null;
	public String user(AddBookbean bookBean)
	{
            Connection conn = null;
            String bookName = bookBean.getbookName();
            String authorName = bookBean.getauthorName();
            int categoryId =  Integer.parseInt(bookBean.getcategoryId());
            String bookDescription = bookBean.getbookdescription();
            int bookPrice= Integer.parseInt(bookBean.getBookPrice());
           // int bookPrice= 100;
            int totalBooks = Integer.parseInt(bookBean.gettotalBooks());
            int booksSold = Integer.parseInt(bookBean.getbooksSold());
            int availableBooks = Integer.parseInt(bookBean.getavailableBooks());
           InputStream image=bookBean.getBytes();
           System.out.println("bookName add book dao entered "+bookName);
           System.out.println("authorName add book dao entered"+authorName);
           
           System.out.println("categoryId add book dao entered"+categoryId);
           System.out.println("bookDescription add book dao entered"+bookDescription);
           
           System.out.println("bookPrice add book dao entered"+bookPrice);
           System.out.println("totalBooks add book dao entered"+totalBooks);
           
           System.out.println("booksSold add book dao entered"+booksSold);
           System.out.println("availableBooks add book dao entered"+availableBooks);
            try
            {
                conn = DbConnection.conn();
			System.out.println("add book dao entered"+conn);
			//String qry="insert into books121(book_id,book_name,author_name,category_id,image,bookdescription,book_Price,total_no_of_books,no_of_books_sold,available_books) values (BOOKSEQUENCE.nextval,?,?,?,?,?,?,?,?,?)";
			String qry="insert into books121(book_name,author_name,category_id,bookdescription,book_Price,total_no_of_books,no_of_books_sold,available_books,image) values (?,?,?,?,?,?,?,?,?)";
                        //String qry= "insert into booktest(book_id,book_name) values (BOOKSEQUENCE.nextval,?)";
                        //String qry="insert into customers(customer_id,email_id,phonenumber,username,password,confirm_password) values (customer_id.nextval,?,?,?,?,?)";
                        System.out.println("after2222");
                        PreparedStatement st = conn.prepareStatement(qry);
                        st.setString(1, bookName);
                        st.setString(2, authorName);
                        st.setInt(3, categoryId);
                        st.setString(4, bookDescription);
                        st.setInt(5, bookPrice);
                        st.setInt(6, totalBooks);
                        st.setInt(7, booksSold);
                        st.setInt(8, availableBooks);
                        st.setBlob(9,image);
                        st.executeUpdate();
                       return "SUCCESS";
		
            
            }
            catch(Exception e)
            {
                System.out.println("exception in add book exception:"+e.getMessage() );
                System.out.println("exception in add book exception:"+e.getStackTrace() );
            }
            return "UNSUCCESS";
        
        }
    
}
